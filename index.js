const {Telegraf, Markup} = require('telegraf');
const mongoose = require('mongoose');
const config = require('config');
const Products = require('./models/products');

const bot = new Telegraf(config.get('tgToken'));


const start = async () => {
    try {
        await mongoose.connect(config.get('mongoURL'), {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        });
        console.log("БАЗА ДАННЫХ ПОДКЛЮЧЕНА.");
    } catch (e) {
        console.log(e.message);
    }

    try {
        let id;
        let cals = 0;
        bot.start(async (ctx) => {
            await ctx.reply(`Привет, ${ctx.from.first_name}.`);
            await ctx.reply("Команды: \n/start - запуск бота. \n/find \"название продукта\" \"Вес в гр.\" - поиск продуктов. \n/cal - Ваши каллории. \n/rcal - обнулить каллории.");
            id = ctx.chat.id;
            console.log(`Подключен чат. ID CHAT: ${id}`);

            await bot.command('find', async (ctx) => {
                try {
                    const find = ctx.update.message.text.split(" ")[1];
                    const mass = ctx.update.message.text.split(" ")[2] || 100;

                    if(find) {
                        try {
                            await Products.find({"Name": {$regex: find, $options: 'i'}}, (err, result) => {
                                if (result.length > 0) {
                                    ctx.reply("Выберите нужный вариант: ", Markup.inlineKeyboard(
                                        result.map(item => [{
                                            text: item.Name,
                                            callback_data: `${item.Name},${item.Calorie},${item.Protein},${mass}`
                                        }])
                                    ).resize().extra());
                                } else {
                                    ctx.reply('Увы, сейчас я не знаю такого продукта, \nзапрос на добавление отправлен разработчику.')
                                    console.log("Отсутсвие товара: ", ctx.update.message.text);
                                }
                            });
                        } catch (e) {
                            console.log(e.message);
                        }
                    } else {
                        await ctx.reply("Вы не ввели название продукта.");
                    }
                } catch (e) {
                    await ctx.reply(e.message);
                }
            });

            bot.on('callback_query', ctx => {
                const arr = ctx.update.callback_query.data.split(',');
                const [name, cal, protein, mass] = arr;
                const calOnMass = (cal / 100) * mass;
                const proteinOnMass = (protein / 100) * mass;
                ctx.reply(`\nПродукт: ${name} 
                                \nКаллории: ${cal} кал.
                                \nКаллории (на ${mass}гр.): ${calOnMass} кал.
                                \nБелок: ${protein}гр.
                                \nБелок (на ${mass}гр.): ${proteinOnMass}`);
                cals += calOnMass;
            });

            bot.command("cal", (ctx) => {
                ctx.reply(`Ваши каллории: ${cals} кал.`);
            });

            bot.command("rcal", (ctx) => {
                ctx.reply(`Каллории обнулены.`);
                cals = 0;
            });
        });
        await bot.launch();
    } catch (e) {
        console.log('Server Error', e.message);
    }
};

start();