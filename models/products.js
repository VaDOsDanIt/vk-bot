const {Schema} = require('mongoose');
const Mongoose = require('mongoose');


const Products = new Schema({
    Name: String,
    Protein: Number,
    Fats: Number,
    Carbohydrates: Number,
    Calorie: Number,
});

module.exports = Mongoose.model('Products', Products);