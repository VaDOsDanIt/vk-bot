const {Schema} = require('mongoose');
const Mongoose = require('mongoose');

const User = new Schema({
    tg_id: {type: Number, unique: true},
    first_name: String,
    last_name: String,
    tg_nickname: String,
    calories: Number,

});

module.exports = Mongoose.model('User', User);